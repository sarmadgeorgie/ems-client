import { mapValues, pickBy, isNil } from 'lodash';

/**
 * Base query params type
 *
 * @export
 * @interface IQueryParams
 */
export interface IQueryParams {
    [k: string]: string|number|boolean
}

/**
 * Query params object with all values stringified
 *
 * @export
 * @interface IStringifiedQueryParams
 */
export interface IStringifiedQueryParams {
    [k: string]: string
}

/**
 * A utility class to manipulate query params objects
 *
 * @export
 * @class QueryParams
 */
export class QueryParams {

    /**
     * Convert all query values to strings
     *
     * @private
     * @static
     * @param {IQueryParams} params
     * @returns {IStringifiedQueryParams}
     * @memberof QueryParams
     */
    private static stringify(params: IQueryParams): IStringifiedQueryParams {
        return mapValues(params, val => String(val));
    }

    /**
     * Pluck empty values
     *
     * @private
     * @static
     * @param {IQueryParams} params
     * @returns {IQueryParams}
     * @memberof QueryParams
     */
    private static removeNils(params: IQueryParams): IQueryParams {
        return pickBy(params, (v, k) => (!isNil(v)) && v !== '');
    }

    /**
     * Create a stringified and denilled query params object
     *
     * @static
     * @param {IQueryParams} params
     * @returns {IStringifiedQueryParams}
     * @memberof QueryParams
     */
    static create(params: IQueryParams): IStringifiedQueryParams {
        const deNilled = QueryParams.removeNils(params);

        return QueryParams.stringify(deNilled);
    }
}
