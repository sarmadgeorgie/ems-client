import { IQueryParams } from './Query';

/**
 * Query options of the employees search endpoint
 *
 * @export
 * @interface IEmployeesQuery
 * @extends {IQueryParams}
 */
export interface IEmployeesQuery extends IQueryParams {
    limit?:         number,
    page?:          number,
    name?:          string,
    department_id:  string
};

/**
 * Query options of the single employee fetch endpoing
 *
 * @export
 * @interface ISingleEmployeeQuery
 * @extends {IQueryParams}
 */
export interface ISingleEmployeeQuery extends IQueryParams {
    "roles"?:         1|0,
    "department"?:    1|0
};

/**
 * Sign in credentials
 *
 * @export
 * @interface IEmployeeCredentials
 */
export interface IEmployeeCredentials {
    email:          string,
    password:       string
};

/**
 * EMS Employee
 *
 * @export
 * @interface IEmployee
 */
export interface IEmployee {
    id?:            number;
    email:          string;
    password?:      string;
    first_name:     string;
    last_name:      string;
    department_id:  number;
    department?:    import('./Department').IDepartment;
    roles?:         import('./Role').IRole[];
    roles_ids?:     number[];
}
