
/**
 * EMS role type
 *
 * @export
 * @interface IRole
 */
export interface IRole {
    id?:            number;
    name:           string;
    commonName:     string;
}
