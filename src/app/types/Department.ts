import { IQueryParams } from './Query';

/**
 * Query options of the departments search endpoint
 *
 * @export
 * @interface IDepartmentsQuery
 * @extends {IQueryParams}
 */
export interface IDepartmentsQuery extends IQueryParams {
    limit?:         number,
    page?:          number,
    name?:          string,
    sortBy?:        'id'|'name',
    sortOrder?:     'asc'|'desc',
    userCount?:    1|0
};

/**
 * Query options of the single department fetch endpoint
 *
 * @export
 * @interface ISingleDepartmentQuery
 * @extends {IQueryParams}
 */
export interface ISingleDepartmentQuery extends IQueryParams {
    usersCount?:    1|0
};

/**
 * An EMS department
 *
 * @export
 * @interface IDepartment
 */
export interface IDepartment {
    id?:                number;
    name:               string;
    employees?:         import('./Employee').IEmployee[];
    users_count?:       number;
}
