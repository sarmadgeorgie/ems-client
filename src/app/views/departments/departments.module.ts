import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DepartmentsComponent } from './departments.component';
import { Routes, RouterModule } from '@angular/router';
import { DepartmentsLandingComponent } from './components/departments-landing/departments-landing.component';
import { DepartmentFormComponent } from './components/department-form/department-form.component';
import { DepartmentsSingleComponent } from './components/departments-single/departments-single.component';
import { SharedModule } from '../../shared/shared.module';
import { RBACGuard } from '../../guards';

const routes: Routes = [
  {
    path: '',
    component: DepartmentsComponent,
    data: {
      title: 'Departments'
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        component: DepartmentsLandingComponent,
        data: {
          title: 'All'
        }
      },
      {
        path: 'add',
        component: DepartmentFormComponent,
        data: {
          title: 'Add'
        }
      },
      {
        path: ':id',
        component: DepartmentsSingleComponent,
        data: {
          title: 'Details'
        }
      },
      {
        path: ':id/edit',
        component: DepartmentFormComponent,
        canActivate: [RBACGuard],
        data: {
          title: 'Edit',
          allowedRoles: ['superadmin']
        }
      },
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ],
  declarations: [
    DepartmentsComponent,
    DepartmentsLandingComponent,
    DepartmentsSingleComponent,
    DepartmentFormComponent
  ]
})
export class DepartmentsModule { }
