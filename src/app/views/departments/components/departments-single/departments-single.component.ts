import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { IDepartment } from '../../../../types';
import { DepartmentsService, AuthService } from '../../../../services';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-departments-single',
  templateUrl: './departments-single.component.html',
  styleUrls: ['./departments-single.component.scss']
})
export class DepartmentsSingleComponent implements OnInit {
  private subs: Subscription[] = [];

  department: IDepartment;
  loading: boolean = true;
  deleting: boolean = false;

  canDelete: boolean = false;
  canEdit: boolean = false;

  constructor(
      private auth: AuthService,
      private departmentsRepo: DepartmentsService,
      private route: ActivatedRoute,
      private router: Router
  ) { }

  ngOnInit() {
      const id = this.route.snapshot.params.id;

      const sub = this.departmentsRepo.getDepartment(+id, { userCount: 1 })
          .subscribe(department => {
              this.department = department;
              this.loading = false;
          });

      this.subs.push(sub);

      this.canDelete = this.canEdit = this.auth.hasRole(['superadmin']);
  }

  onDelete() {
    this.departmentsRepo
        .deleteDepartment(this.department.id)
        .subscribe(result => {
            if (result) {
                this.router.navigate(['/departments']);
            } else {
                this.deleting = false;
            }
        });
  }

  ngOnDestroy() {
      this.subs.forEach(sub => sub.unsubscribe());
  }
}
