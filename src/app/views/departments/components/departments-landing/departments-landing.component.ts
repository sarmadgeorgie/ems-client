import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { remove } from 'lodash';
import { FormGroup, FormControl } from '@angular/forms';
import { DepartmentsService } from '../../../../services';
import { IDepartment } from '../../../../types';

@Component({
  selector: 'app-departments-landing',
  templateUrl: './departments-landing.component.html',
  styleUrls: ['./departments-landing.component.scss']
})
export class DepartmentsLandingComponent implements OnInit {
    departments: IDepartment[];

    queryForm = new FormGroup({
      name: new FormControl('')
    });

    limit = 50;
    page = 1;

    endOfResults: boolean = false;

    pendingDelete: number[] = [];

    constructor(
      private departmentsRepo: DepartmentsService
    ) { }

    ngOnInit() {
      this.search();
    }

    search = (nextPage: boolean = false) => {
      this.departmentsRepo
        .searchDepartments({})
        .subscribe(departments => {
          this.departments = departments;
        });
    }

    onScrolled() {
        this.search(true);
    }

    onClearFilter() {
        this.queryForm.reset();
        this.search();
    }
}
