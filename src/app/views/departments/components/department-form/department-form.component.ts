import { Component, OnInit } from '@angular/core';
import { IDepartment } from '../../../../types';
import { Subscription, Observable } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DepartmentsService } from '../../../../services';
import { ActivatedRoute, Router } from '@angular/router';
import { pick } from 'lodash';

@Component({
  selector: 'app-department-form',
  templateUrl: './department-form.component.html',
  styleUrls: ['./department-form.component.scss']
})
export class DepartmentFormComponent implements OnInit {
  private subs: Subscription[] = [];

  department: IDepartment;
  adding: boolean = false;
  loadingDept: boolean = false;
  submitting: boolean = false;

  deptForm = new FormGroup({
      id: new FormControl(''),
      name: new FormControl('', [Validators.required])
  });

  get id() { return this.deptForm.get('id'); }
  get name() { return this.deptForm.get('name'); }

  constructor(
      private departmentsRepo: DepartmentsService,
      private route: ActivatedRoute,
      private router: Router
  ) { }

  ngOnInit() {
      const id = this.route.snapshot.params.id;
      this.adding = id === undefined;

      if (!this.adding) {
          this.loadingDept = true;
          const deptSub = this.departmentsRepo
              .getDepartment(id, { usersCount: 0 })
              .subscribe(department => {
                  this.loadingDept = false;
                  this.department = department;

                  this.deptForm.setValue(pick(department, ...['id', 'name']));
              });

          this.subs.push(deptSub);
      }
  }

  onSubmit() {
      const department = this.deptForm.value;
      department.id = +department.id;

      let req: Observable<IDepartment>;

      if (this.adding) {
          req = this.departmentsRepo.createDepartment(department);
      } else {
          req = this.departmentsRepo.updateDepartment(department);
      }

      const sub = req.subscribe(department => {
          if (department && department.id) {
              this.router.navigate(['/departments', department.id]);
          }

          this.submitting = false;
      });

      this.subs.push(sub);
  }

  ngOnDestroy() {
      this.subs.forEach(sub => sub.unsubscribe());
  }
}
