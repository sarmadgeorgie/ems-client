import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { IDepartment } from '../../types';
import { DepartmentsService } from '../../services';

@Component({
  templateUrl: 'dashboard.component.html'
})
export class DashboardComponent implements OnInit {
  departments: IDepartment[];

  // public pieChartLabels: string[] = ['Download Sales', 'In-Store Sales', 'Mail Sales'];
  // public pieChartData: number[] = [300, 500, 100];
  public pieChartType = 'pie';

  get pieChartLabels() { return this.departments.map(d => d.name); }
  get pieChartData() { return this.departments.map(d => d.users_count); }


  constructor(
    private departmentsRepo: DepartmentsService
  ) {}

  ngOnInit() {
    this.departmentsRepo
      .searchDepartments({ userCount: 1 })
      .subscribe(departments => {
        this.departments = departments;
      });
  }

  // events
  public chartClicked(e: any): void {
  }

  public chartHovered(e: any): void {
  }
}
