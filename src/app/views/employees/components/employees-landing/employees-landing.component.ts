import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { IEmployee, IDepartment, IEmployeesQuery } from '../../../../types';
import { EmployeesService, DepartmentsService } from '../../../../services';
import { keyBy, remove } from 'lodash';

@Component({
    selector: 'app-employees-landing',
    templateUrl: './employees-landing.component.html',
    styleUrls: ['./employees-landing.component.scss']
})
export class EmployeesLandingComponent implements OnInit {
    private lastQueryOpts: IEmployeesQuery;

    employees: IEmployee[] = [];
    departments: Observable<IDepartment[]>;
    endOfResults: boolean = false;

    pendingDelete: number[] = [];

    departmentsMap: { [id: number]: IDepartment } = {};

    queryForm = new FormGroup({
        name: new FormControl(''),
        department_id: new FormControl('')
    });

    limit = 30;
    page = 1;

    constructor(
        private employeesRepo: EmployeesService,
        private departmentsRepo: DepartmentsService
    ) { }

    ngOnInit() {
        this.getDepartments();
        this.search();
    }

    getDepartments() {
        const deps = this.departments = this.departmentsRepo.searchDepartments({
            limit: 50,
            page: 1
        });

        deps.subscribe(depsArr => {
            this.departmentsMap = keyBy(depsArr, 'id');
        });
    }

    search = (nextPage: boolean = false) => {
        if (nextPage && this.endOfResults) { return; }

        let queryOpts: IEmployeesQuery;

        if (nextPage) {
            this.lastQueryOpts.page += 1;
            queryOpts = this.lastQueryOpts;
        } else {
            this.lastQueryOpts = queryOpts = {
                ...this.queryForm.value,
                limit: this.limit,
                page: this.page
            }
        }

        this.employeesRepo
            .searchEmployees(queryOpts)
            .subscribe(employees => {
                if (nextPage) {
                    this.employees = this.employees.concat(employees);
                } else {
                    this.employees = employees;
                }

                this.endOfResults = employees.length < queryOpts.limit;
            });
    }

    onScrolled() {
        this.search(true);
    }

    onClearFilter() {
        this.queryForm.reset();
        this.search();
    }
}
