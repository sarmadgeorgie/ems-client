import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { IEmployee } from '../../../../types';
import { ActivatedRoute, Router } from '@angular/router';
import { EmployeesService, AuthService } from '../../../../services';

@Component({
    selector: 'app-employees-single',
    templateUrl: './employees-single.component.html',
    styleUrls: ['./employees-single.component.scss']
})
export class EmployeesSingleComponent implements OnInit, OnDestroy {
    private subs: Subscription[] = [];

    employee: IEmployee;
    loading: boolean = true;
    deleting: boolean = false;

    canDelete: boolean = false;
    canEdit: boolean = false;

    constructor(
        private auth: AuthService,
        private employeesRepo: EmployeesService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {
        const id = this.route.snapshot.params.id;

        const sub = this.employeesRepo.getEmployee(+id, { roles: 1, department: 1 })
            .subscribe(employee => {
                this.employee = employee;
                this.loading = false;
            });

        this.subs.push(sub);

        this.canDelete = this.canEdit = this.auth.hasRole(['superadmin']);
    }

    onDelete() {
        this.deleting = true;

        this.employeesRepo
            .deleteEmployee(this.employee.id)
            .subscribe(result => {
                if (result) {
                    this.router.navigate(['/employees']);
                }

                this.deleting = false;
            });
    }


    ngOnDestroy() {
        this.subs.forEach(sub => sub.unsubscribe());
    }
}
