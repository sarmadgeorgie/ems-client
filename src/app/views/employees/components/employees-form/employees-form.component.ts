import { Component, OnInit, OnDestroy } from '@angular/core';
import { IEmployee, IDepartment } from '../../../../types';
import { EmployeesService, DepartmentsService } from '../../../../services';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription, Observable } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { pick } from 'lodash';

@Component({
    selector: 'app-employees-form',
    templateUrl: './employees-form.component.html',
    styleUrls: ['./employees-form.component.scss']
})
export class EmployeesFormComponent implements OnInit, OnDestroy {
    private subs: Subscription[] = [];

    employee: IEmployee;
    departments: IDepartment[];
    adding: boolean = false;
    loadingEmp: boolean = false;
    loadingDept: boolean = true;
    submitting: boolean = false;

    empForm = new FormGroup({
        id: new FormControl(''),
        email: new FormControl('', [Validators.email, Validators.required]),
        first_name: new FormControl('', [Validators.required]),
        last_name: new FormControl('', [Validators.required]),
        department_id: new FormControl('', [Validators.required])
    });

    get id() { return this.empForm.get('id'); }
    get email() { return this.empForm.get('email'); }
    get first_name() { return this.empForm.get('first_name'); }
    get last_name() { return this.empForm.get('last_name'); }
    get department_id() { return this.empForm.get('department_id'); }

    constructor(
        private employeesRepo: EmployeesService,
        private departmentsRepo: DepartmentsService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {
        const id = this.route.snapshot.params.id;
        this.adding = id === undefined;

        const deptSub = this.departmentsRepo
            .searchDepartments({})
            .subscribe(departments => {
                this.loadingDept = false;
                this.departments = departments;
            });

        this.subs.push(deptSub);

        if (!this.adding) {
            this.loadingEmp = true;
            const empSub = this.employeesRepo
                .getEmployee(id)
                .subscribe(employee => {
                    this.loadingEmp = false;
                    this.employee = employee;

                    this.empForm.setValue(pick(employee, ...['id', 'email', 'first_name', 'last_name', 'department_id']));
                });

            this.subs.push(empSub);
        }
    }

    onSubmit() {
        const employee = this.empForm.value;
        employee.id = +employee.id;
        employee.department_id = +employee.department_id;

        let req: Observable<IEmployee>;

        if (this.adding) {
            req = this.employeesRepo.createEmployee(employee);
        } else {
            req = this.employeesRepo.updateEmployee(employee);
        }

        const sub = req.subscribe(employee => {
            if (employee && employee.id) {
                this.router.navigate(['/employees', employee.id]);
            }

            this.submitting = false;
        });

        this.subs.push(sub);
    }

    ngOnDestroy() {
        this.subs.forEach(sub => sub.unsubscribe());
    }
}
