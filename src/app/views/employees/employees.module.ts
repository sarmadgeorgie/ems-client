import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeesComponent } from './employees.component';
import { EmployeesLandingComponent } from './components/employees-landing/employees-landing.component';
import { EmployeesSingleComponent } from './components/employees-single/employees-single.component';
import { EmployeesFormComponent } from './components/employees-form/employees-form.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { RBACGuard } from '../../guards';

const routes: Routes = [
  {
    path: '',
    component: EmployeesComponent,
        data: {
          title: 'Employees'
        },
    children: [
      {
        path: '',
        pathMatch: 'full',
        component: EmployeesLandingComponent,
        data: {
          title: 'All'
        },
      },
      {
        path: 'add',
        component: EmployeesFormComponent,
        data: {
          title: 'Add'
        }
      },
      {
        path: ':id',
        component: EmployeesSingleComponent,
        data: {
          title: 'Details'
        }
      },
      {
        path: ':id/edit',
        component: EmployeesFormComponent,
        canActivate: [RBACGuard],
        data: {
          title: 'Edit',
          allowedRoles: ['superadmin']
        }
      },
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ],
  declarations: [
    EmployeesComponent,
    EmployeesLandingComponent,
    EmployeesSingleComponent,
    EmployeesFormComponent
  ]
})
export class EmployeesModule { }
