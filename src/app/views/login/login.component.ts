import { Component } from '@angular/core';
import { AuthService } from '../../services';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})
export class LoginComponent {

    loading = false;
    error: string;

    authForm = new FormGroup({
        email: new FormControl('', [ Validators.email, Validators.required ]),
        password: new FormControl('', [ Validators.required ])
    });

    get email() { return this.authForm.get('email'); }
    get password() { return this.authForm.get('password'); }

    constructor(
        private auth: AuthService,
        private router: Router
    ) {}

    onLogin() {
        this.loading = true;

        this.auth.login(this.authForm.value)
            .pipe(
                catchError(err => of(err))
            )
            .subscribe(response => {
                if (response['error']) {
                    this.error = response['message'] || 'Error while trying to sign in';
                } else {
                    this.router.navigate(['/']);
                }
                this.loading = false;
            })
    }
}
