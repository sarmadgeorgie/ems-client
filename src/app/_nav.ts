export const navItems = [
  {
    name: 'Dashboard',
    url: '/dashboard',
    icon: 'icon-speedometer'
  },
  {
    name: 'Departments',
    url: '/departments',
  },
  {
    name: 'Employees',
    url: '/employees'
  },
];
