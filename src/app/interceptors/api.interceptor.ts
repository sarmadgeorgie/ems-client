import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { AuthService } from '../services';

/**
 * Injects the API url for all outgoing requests
 *
 * @export
 * @class APIInterceptor
 * @implements {HttpInterceptor}
 */
@Injectable()
export class APIInterceptor implements HttpInterceptor {

    constructor(
        private auth: AuthService
    ) {}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const options = {
            url: `${environment.apiUrl}/${req.url}`
        };

        const apiReq = req.clone(options);

        return next.handle(apiReq);
    }
}