import { Injectable } from '@angular/core';
import { IDepartment, IDepartmentsQuery, QueryParams, ISingleDepartmentQuery } from '../types';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class DepartmentsService {

    constructor(
        private http: HttpClient
    ) { }

    /**
     * Get a list of departments
     *
     * @param {IDepartmentsQuery} q
     * @returns
     * @memberof DepartmentsService
     */
    searchDepartments(q: IDepartmentsQuery) {
        const params = QueryParams.create(q);

        return this.http
            .get<IDepartment[]>('departments', { params });
    }

    /**
     * Get a single department by id
     *
     * @param {number} id
     * @param {ISingleDepartmentQuery} q
     * @returns {Observable<IDepartment[]>}
     * @memberof DepartmentsService
     */
    getDepartment(id: number, q: ISingleDepartmentQuery) {
        const params = QueryParams.create(q);

        return this.http
            .get<IDepartment>(`departments/${id}`, { params });
    }

    /**
     * Create a new department
     *
     * @param {IDepartment} department
     * @returns {Observable<IDepartment>}
     * @memberof DepartmentsService
     */
    createDepartment(department: IDepartment) {
        return this.http
            .post<IDepartment>('departments', department);
    }

    /**
     * Update an existing department
     *
     * @param {IDepartment} department
     * @returns {Observable<IDepartment>}
     * @memberof DepartmentsService
     */
    updateDepartment(department: IDepartment) {
        return this.http
            .put<IDepartment>(`departments/${department.id}`, department);
    }

    /**
     * Delete a department
     *
     * @param {number} id
     * @returns {Observable<number>} number of deleted departments
     * @memberof DepartmentsService
     */
    deleteDepartment(id: number) {
        return this.http
            .delete(`departments/${id}`);
    }

}
