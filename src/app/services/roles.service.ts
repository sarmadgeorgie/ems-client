import { Injectable } from '@angular/core';
import { IRole } from '../types';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class RolesService {

    constructor(
        private http: HttpClient
    ) { }

    getAllRoles() {
        return this.http
            .get<IRole[]>('roles');
    }
}
