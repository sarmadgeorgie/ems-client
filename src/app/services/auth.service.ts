import { Injectable } from '@angular/core';
import { IEmployeeCredentials, IEmployee, IRole } from '../types';
import { HttpClient } from '@angular/common/http';
import { tap, finalize } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    public get token(): string {
        return localStorage.getItem('token') || null;
    }
    public set token(v: string) {
        this.loggedIn.next(v !== null);
        localStorage.setItem('token', v);
    }

    public user: BehaviorSubject<IEmployee> = new BehaviorSubject<IEmployee>(null);
    public loggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(this.token !== null);
    public loggingIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

    constructor(
        private http: HttpClient,
        private jwtHelper: JwtHelperService
    ) { }

    /**
     * Login with email and password
     *
     * @param {IEmployeeCredentials} creds
     * @returns {Observable}
     * @memberof AuthService
     */
    login(creds: IEmployeeCredentials) {
        this.loggingIn.next(true);

        const req = this.http
            .post('auth/login', creds)
            .pipe(
                tap(this.loginResponseHandler.bind(this)),
                finalize(() => this.loggingIn.next(false))
            );

        return req;
    }

    /**
     * Check if token is present and not expired
     *
     * @returns {boolean}
     * @memberof AuthService
     */
    public isAuthenticated(): boolean {
        return !this.jwtHelper.isTokenExpired(this.token);
    }

    loginResponseHandler(res) {
        if (res && !res.error) {
            this.token = res.token || null;
            this.user.next(res.user);
        }
    }

    /**
     * Deletes token from storage
     *
     * @memberof AuthService
     */
    logout() {
        this.token = null;
    }

    /**
     * Check if the current signed in user has at least on of the requested role
     *
     * @param {string[]} roles
     * @returns {boolean}
     * @memberof AuthService
     */
    hasRole(roles: string[]): boolean {

        // decode the token to get its payload
        const payload = this.jwtHelper.decodeToken(this.token);

        const attachedRoles = payload.roles.map(role => role.name);

        for (let role of attachedRoles) {
            if (roles.indexOf(role) >= 0) {
                return true;
            }
        }

        return false;
    }
}
