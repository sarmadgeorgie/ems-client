import { Injectable } from '@angular/core';
import { IEmployeesQuery, IEmployee, ISingleEmployeeQuery, QueryParams } from '../types';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class EmployeesService {

    constructor(
        private http: HttpClient
    ) { }

    /**
     * Get a list of employees
     *
     * @param {IEmployeesQuery} q
     * @returns {Observable<IEmployee[]>}
     * @memberof EmployeesService
     */
    searchEmployees(q: IEmployeesQuery) {
        const params = QueryParams.create(q);

        return this.http
            .get<IEmployee[]>('users', { params });
    }

    /**
     * Get an employee by id
     *
     * @param {number} id
     * @param {ISingleEmployeeQuery} [q]
     * @returns {Observable<IEmployee>}
     * @memberof EmployeesService
     */
    getEmployee(id: number, q?: ISingleEmployeeQuery) {
        q = q || {};
        const params = QueryParams.create(q);
        return this.http
            .get<IEmployee>(`users/${id}`, { params });
    }

    /**
     * Create a new employee
     *
     * @param {IEmployee} employee
     * @returns {Observable<IEmployee>}
     * @memberof EmployeesService
     */
    createEmployee(employee: IEmployee) {
        return this.http
            .post<IEmployee>('users', employee);
    }

    /**
     * Update an existing employee
     *
     * @param {IEmployee} employee
     * @returns {Observable<IEmployee>}
     * @memberof EmployeesService
     */
    updateEmployee(employee: IEmployee) {
        return this.http
            .put<IEmployee>(`users/${employee.id}`, employee);
    }

    /**
     * Delete an employee
     *
     * @param {number} id
     * @returns {Observable<number} number of deleted employees
     * @memberof EmployeesService
     */
    deleteEmployee(id: number) {
        return this.http
            .delete(`users/${id}`);
    }

}
