import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services';
import { Router } from '@angular/router';

@Component({
  selector: 'app-rich-header',
  templateUrl: './rich-header.component.html',
  styleUrls: ['./rich-header.component.scss']
})
export class RichHeaderComponent implements OnInit {

  constructor(
    private auth: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  onLogout() {
    this.auth.logout();
    this.router.navigate(['/login']);
  }

}
