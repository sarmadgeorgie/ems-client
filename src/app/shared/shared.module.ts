import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmsBreadcrumbComponent } from './components/ems-breadcrumb/ems-breadcrumb.component';
import { EmsFooterComponent } from './components/ems-footer/ems-footer.component';
import { EmsSidebarComponent } from './components/ems-sidebar/ems-sidebar.component';
import { RichHeaderComponent } from './components/rich-header/rich-header.component';
import { RouterModule } from '@angular/router';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import {
  AppAsideModule,
  AppBreadcrumbModule,
  AppHeaderModule,
  AppFooterModule,
  AppSidebarModule,
} from '@coreui/angular';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    BsDropdownModule,
    TabsModule,
    ChartsModule,
    AppAsideModule,
    AppBreadcrumbModule,
    AppHeaderModule,
    AppFooterModule,
    AppSidebarModule,
    PerfectScrollbarModule,
    ReactiveFormsModule,
    FormsModule,
    InfiniteScrollModule
  ],
  exports: [
    EmsBreadcrumbComponent,
    EmsFooterComponent,
    EmsSidebarComponent,
    RichHeaderComponent,
    BsDropdownModule,
    TabsModule,
    ChartsModule,
    AppAsideModule,
    AppBreadcrumbModule,
    AppHeaderModule,
    AppFooterModule,
    AppSidebarModule,
    PerfectScrollbarModule,
    ReactiveFormsModule,
    FormsModule,
    InfiniteScrollModule
  ],
  declarations: [
    EmsBreadcrumbComponent,
    EmsFooterComponent,
    EmsSidebarComponent,
    RichHeaderComponent
  ]
})
export class SharedModule { }
