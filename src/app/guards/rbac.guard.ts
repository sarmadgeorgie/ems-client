import { Injectable } from '@angular/core';
import {
  Router,
  CanActivate,
  ActivatedRouteSnapshot,
  CanActivateChild,
  RouterStateSnapshot
} from '@angular/router';
import { AuthService } from '../services';
import { JwtHelperService } from '@auth0/angular-jwt';

/**
 * Route guard based on allowed roles per route
 *
 * @export
 * @class RBACGuard
 * @implements {CanActivate}
 * @implements {CanActivateChild}
 */
@Injectable()
export class RBACGuard implements CanActivate, CanActivateChild {

  constructor(
      public auth: AuthService,
      public router: Router,
      public jwtHelper: JwtHelperService
    ) {}

  canActivate(route: ActivatedRouteSnapshot): boolean {
    return this.rbacHandler(route);
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    return this.rbacHandler(route);
  }

  private rbacHandler(route: ActivatedRouteSnapshot): boolean {
    const allowedRoles = route.data.allowedRoles;

    if (this.auth.hasRole(allowedRoles)) {
      return true;
    }

    this.router.navigate(['/login']);
    return false;
  }
}