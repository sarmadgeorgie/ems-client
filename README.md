# Employee Management System - Angular Client

## Setup
After cloning the project, install dependencies with the following command:
```
npm install
```
Once that is done, you can run a live local server on http://localhost:4200 by typing:
```
npm start
```